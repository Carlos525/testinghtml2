package mx.tec.lab;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import org.springframework.boot.test.context.SpringBootTest;

import com.gargoylesoftware.htmlunit.html.HtmlListItem;

import java.util.concurrent.TimeUnit;

@SpringBootTest
public class TestHtml2ApplicationTests {
	private static WebDriver driver;
	
	@BeforeAll
	public static void setUp() {
		System.setProperty("webdriver.chrome.driver", "D:\\Educacion\\Profesional\\Semestre6\\CPS\\Lab9\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}
	
	@AfterAll
	public static void tearDown() {
		driver.quit();
	}
	
	@Test
	public void givenAClient_whenEnteringAutomationPractice_thenPageTitleIsCorrect() throws Exception {
		//When
		driver.get("http://automationpractice.com/index.php");
		String title = driver.getTitle();
		
		//Then
		assertEquals("My Store", title);
	}
	
	@Test
	public void givenAClient_whenEnteringLoginCredentials_thenAccountPageIsDisplayed() throws Exception{
		//When
		driver.get("http://automationpractice.com/index.php?controller=authentication&back=my-account");
		WebElement emailField = driver.findElement(By.id("email"));
		emailField.sendKeys("carlos.estradaih@gmail.com");
		WebElement passwordField = driver.findElement(By.id("passwd"));
		passwordField.sendKeys("Contrasenia");
		WebElement submitButton = driver.findElement(By.id("SubmitLogin"));
		submitButton.click();
		String title = driver.getTitle();
		
		//Then
		assertEquals("My account - My Store", title);
	}
	
	@Test
	public void givenAClient_whenEnteringWrongCredentials_thenAuthenticationPageIsDisplayed() throws Exception{
		//When
		driver.get("http://automationpractice.com/index.php?controller=authentication&back=my-account");
		WebElement emailField = driver.findElement(By.id("email"));
		emailField.sendKeys("carlos.estradaih@gmail.com");
		WebElement passwordField = driver.findElement(By.id("passwd"));
		passwordField.sendKeys("wrongpassword");
		WebElement submitButton = driver.findElement(By.id("SubmitLogin"));
		submitButton.click();
		String title = driver.getTitle();
		
		//Then
		assertEquals("Login - My Store", title);
	}
	
	@Test
	public void givenAClient_whenEnteringWrongCredentials_thenErrorMessageIsDisplayed() throws Exception{
		//When
		driver.get("http://automationpractice.com/index.php?controller=authentication&back=my-account");
		WebElement emailField = driver.findElement(By.id("email"));
		emailField.sendKeys("carlos.estradaih@gmail.com");
		WebElement passwordField = driver.findElement(By.id("passwd"));
		passwordField.sendKeys("wrongpassword");
		WebElement submitButton = driver.findElement(By.id("SubmitLogin"));
		submitButton.click();
		WebElement alert = driver.findElement(By.xpath("//div[@class='alert alert-danger']/ol/li"));
		//Then
		assertEquals("Authentication failed.", alert.getText());
	}
	
	@Test
	public void givenAClient_whenSearchingNonExistingProduct_thenNoResultDisplayed() throws Exception{
		//When
		driver.get("http://automationpractice.com/index.php");
		WebElement buscador = driver.findElement(By.id("search_query_top"));
		buscador.sendKeys("smartphones");
		WebElement buscar = driver.findElement(By.name("submit_search"));
		buscar.click();
		WebElement resultado = driver.findElement(By.xpath("//span[@class = 'heading-counter']"));
		//Then
		assertEquals("0 results have been found.", resultado.getText());
	}
	
	@Test
	public void givenAClient_whenSearchingEmptyString_thenPleaseEnterDisplayed() throws Exception{
		//When
		driver.get("http://automationpractice.com/index.php");
		WebElement buscar = driver.findElement(By.name("submit_search"));
		buscar.click();
		WebElement resultado = driver.findElement(By.xpath("//p[@class= 'alert alert-warning']"));
		//Then
		assertEquals("Please enter a search keyword", resultado.getText());
	}
	
	@Test
	public void givenAClient_whenSigningOut_thenAuthenticationPageIsDisplayed() throws Exception{
		//When
		driver.get("http://automationpractice.com/index.php?controller=authentication&back=my-account");
		WebElement logoutButton = driver.findElement(By.className("logout"));
		logoutButton.click();
		String title = driver.getTitle();
		//Then
		assertEquals("Login - My Store", title);
	}

}
